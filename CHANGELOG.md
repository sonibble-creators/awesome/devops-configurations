# CHANGELOG.md

## 1.0.1 (unreleased)

Features:

- Add Kubernetes Support
- Adding Skaffold Support
- Add the Telepresence Settings

Bugfixs:

## 1.0.0 (2022-02-14)

Main Release:

- Release some template for application
- Microservice support templates
- Some deployment confugured
